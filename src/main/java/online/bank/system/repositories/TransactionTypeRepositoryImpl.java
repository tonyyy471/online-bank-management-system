package online.bank.system.repositories;

import online.bank.system.models.TransactionType;
import online.bank.system.models.utils.Mapper;
import online.bank.system.repositories.utils.ConnectionWrapper;
import online.bank.system.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class TransactionTypeRepositoryImpl implements TransactionTypeRepository {
    @Override
    public Integer getTransactionTypeIdForName(String name) throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getTransactionTypeIdForNameQuery(name));
        }
        return resultSet.first() ? resultSet.getInt("id") : null;
    }

    @Override
    public List<TransactionType> getTransactionTypes() throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getTransactionTypesQuery());
        }
        return Mapper.getInstance().resultSetToTransactionTypes(resultSet);
    }
}
