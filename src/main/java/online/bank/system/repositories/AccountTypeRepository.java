package online.bank.system.repositories;

import online.bank.system.models.AccountType;

import java.sql.SQLException;
import java.util.List;

public interface AccountTypeRepository {
    Integer getAccountTypeIdForName(String name) throws SQLException;

    List<AccountType> getAccountTypes() throws SQLException;
}
