package online.bank.system.repositories;

import online.bank.system.models.Account;
import online.bank.system.models.Transaction;

import java.sql.SQLException;
import java.util.List;

public interface TransactionRepository {
    List<Transaction> getTransactionsForAccount(Account account) throws SQLException;

    void createTransaction(int transactionTypeId, Transaction transaction) throws SQLException;
}
