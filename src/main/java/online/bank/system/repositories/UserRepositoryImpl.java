package online.bank.system.repositories;

import online.bank.system.models.User;
import online.bank.system.models.utils.Mapper;
import online.bank.system.repositories.utils.ConnectionWrapper;
import online.bank.system.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
    @Override
    public List<User> getUsers() throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getUsersQuery());
        }
        return Mapper.getInstance().resultSetToUsers(resultSet);
    }

    @Override
    public User getUser(String email) throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getUserQuery(email));
        }
        return resultSet.first() ? Mapper.getInstance().resultSetToUser(resultSet, true) : null;
    }

    @Override
    public User getUser(int id) throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getUserQuery(id));
        }
        return resultSet.first() ? Mapper.getInstance().resultSetToUser(resultSet, true) : null;
    }


    @Override
    public void createUser(User user) throws SQLException {
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            statement.executeQuery(QueryBuilder.getInstance().createUserQuery(user));
        }
    }

    @Override
    public void updateUser(User user, int id) throws SQLException {
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            String query = QueryBuilder.getInstance().updateUserQuery(user, id);
            if (!query.isEmpty()) statement.executeQuery(query);
        }
    }
}
