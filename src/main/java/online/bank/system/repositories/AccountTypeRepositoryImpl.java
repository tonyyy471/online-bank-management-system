package online.bank.system.repositories;

import online.bank.system.models.AccountType;
import online.bank.system.models.utils.Mapper;
import online.bank.system.repositories.utils.ConnectionWrapper;
import online.bank.system.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class AccountTypeRepositoryImpl implements AccountTypeRepository {
    @Override
    public Integer getAccountTypeIdForName(String name) throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getAccountTypeIdForNameQuery(name));
        }
        return resultSet.first() ? resultSet.getInt("id") : null;
    }

    @Override
    public List<AccountType> getAccountTypes() throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getAccountTypesQuery());
        }
        return Mapper.getInstance().resultSetToAccountTypes(resultSet);
    }
}
