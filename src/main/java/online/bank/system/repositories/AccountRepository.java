package online.bank.system.repositories;

import online.bank.system.models.Account;
import online.bank.system.models.User;

import java.sql.SQLException;
import java.util.List;

public interface AccountRepository {
    List<Account> getAccountsForUser(int userId) throws SQLException;

    Account getAccount(int accountId) throws SQLException;

    void createAccount(int accountTypeId, int currencyTypeId, User user) throws SQLException;
}
