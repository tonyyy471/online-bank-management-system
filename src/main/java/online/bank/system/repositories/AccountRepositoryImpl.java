package online.bank.system.repositories;

import online.bank.system.models.Account;
import online.bank.system.models.User;
import online.bank.system.models.utils.Mapper;
import online.bank.system.repositories.utils.ConnectionWrapper;
import online.bank.system.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class AccountRepositoryImpl implements AccountRepository {
    @Override
    public List<Account> getAccountsForUser(int userId) throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getAccountsForUserQuery(userId));
        }
        return Mapper.getInstance().resultSetToAccountsForUser(resultSet);
    }

    @Override
    public Account getAccount(int accountId) throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getAccountQuery(accountId));
        }
        return resultSet.first() ? Mapper.getInstance().resultSetToAccount(resultSet) : null;
    }

    @Override
    public void createAccount(int accountTypeId, int currencyTypeId, User user) throws SQLException {
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            statement.executeQuery(QueryBuilder.getInstance().createAccountQuery(accountTypeId, currencyTypeId, user));
        }
    }
}
