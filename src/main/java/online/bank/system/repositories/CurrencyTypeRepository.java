package online.bank.system.repositories;

import online.bank.system.models.CurrencyType;

import java.sql.SQLException;
import java.util.List;

public interface CurrencyTypeRepository {
    Integer getCurrencyTypeIdForName(String name) throws SQLException;

    List<CurrencyType> getCurrencyTypes() throws SQLException;

    Double getCurrencyTypeRate(int id) throws SQLException;

    void setCurrencyTypeRate(Integer id, double rateToBaseEUR) throws SQLException;
}
