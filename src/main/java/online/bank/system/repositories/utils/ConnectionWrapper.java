package online.bank.system.repositories.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionWrapper {
    private static ConnectionWrapper connectionWrapper;
    private Connection connection;

    private ConnectionWrapper() throws SQLException {
        initializeConnection();
        chooseDatabase();
    }

    public static ConnectionWrapper getInstance() throws SQLException {
        if (connectionWrapper == null)
            connectionWrapper = new ConnectionWrapper();
        return connectionWrapper;
    }

    public Connection getConnection() {
        return connection;
    }

    private void initializeConnection() throws SQLException {
        this.connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306", "root", "1234");
    }

    private void chooseDatabase() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.executeQuery("use bank");
        }
    }
}
