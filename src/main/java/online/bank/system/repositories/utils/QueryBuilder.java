package online.bank.system.repositories.utils;

import online.bank.system.models.*;
import online.bank.system.models.dtos.AccountDTO;
import online.bank.system.models.dtos.UserDTO;

public class QueryBuilder {
    private static QueryBuilder queryBuilder;

    private QueryBuilder() {
    }

    public static QueryBuilder getInstance() {
        if (queryBuilder == null)
            queryBuilder = new QueryBuilder();
        return queryBuilder;
    }

    public String getUsersQuery() {
        return "select id, email, first_name, last_name from users";
    }

    public String getUserQuery(String email) {
        return "select id, email, password, security_pin, first_name, last_name from users where email = '" + email + "'";
    }

    public String getUserQuery(int id) {
        return "select id, email, password, security_pin, first_name, last_name from users where id = '" + id + "'";
    }

    public String createUserQuery(User user) {
        return "insert into users(email, password, security_pin, first_name, last_name)\n" +
                "values ('" + user.getEmail() + "', '" + user.getPassword() + "'," +
                " '" + user.getSecurityPin() + "', '" + user.getFirstName() + "', '" + user.getLastName() + "')";
    }

    public String updateUserQuery(User user, int id) {
        String email = isNullOrEmpty(user.getEmail()) ? "" : "email = '" + user.getEmail() + "', ";
        String password = isNullOrEmpty(user.getPassword()) ? "" : "password = '" + user.getPassword() + "', ";
        String securityPin = isNullOrEmpty(user.getSecurityPin()) ? "" : "security_pin = '" + user.getSecurityPin() + "', ";
        String firstName = isNullOrEmpty(user.getFirstName()) ? "" : "first_name = '" + user.getFirstName() + "', ";
        String lastName = isNullOrEmpty(user.getLastName()) ? "" : "last_name = '" + user.getLastName() + "', ";

        String setClause = email + password + securityPin + firstName + lastName;

        return setClause.isEmpty() ? "" : "update users set " + setClause.substring(0, setClause.length() - 2) + " where id = " + id;
    }

    public String createAccountQuery(int accountTypeId, int currencyTypeId, User user) {
        return "insert into accounts(account_type_id, currency_type_id, user_id)\n" +
                "values ('" + accountTypeId + "', '" + currencyTypeId + "'," +
                " '" + ((UserDTO) user).getId() + "')";
    }

    public String getAccountTypesQuery() {
        return "select name from account_types";
    }

    public String getAccountTypeIdForNameQuery(String name) {
        return "select id from account_types where name = '" + name + "'";
    }

    public String getCurrencyTypesQuery() {
        return "select name from currency_types";
    }

    public String getCurrencyTypeIdForNameQuery(String name) {
        return "select id from currency_types where name = '" + name + "'";
    }

    public String getTransactionTypesQuery() {
        return "select name from transaction_types";
    }

    public String getTransactionTypeIdForNameQuery(String name) {
        return "select id from transaction_types where name = '" + name + "'";
    }

    public String getAccountQuery(int id) {
        return "select accounts.id, account_types.name as account_type, currency_types.name as currency_type " +
                "from accounts " +
                "inner join account_types on accounts.account_type_id = account_types.id " +
                "inner join currency_types on accounts.currency_type_id = currency_types.id where accounts.id = " + id;
    }

    public String getAccountsForUserQuery(int id) {
        return "select accounts.id, account_types.name as account_type, currency_types.name as currency_type " +
                "from accounts " +
                "inner join account_types on accounts.account_type_id = account_types.id " +
                "inner join currency_types on accounts.currency_type_id = currency_types.id where user_id = " + id;
    }

    public String createTransactionQuery(int transactionTypeId, Transaction transaction) {
        String optionalReceivingAccountIdColumnName = isTransactionTransfer(transaction) ?
                ", optional_receiving_account_id" : "";
        String optionalReceivingAccountIdColumnValue = isTransactionTransfer(transaction) ?
                ", " + transaction.getOptionalReceivingAccountId() : "";

        String optionalAmountInReceivingAccountCurrencyColumnName = isTransactionTransfer(transaction) ?
                ", amount_in_receiving_account_currency" : "";
        String optionalAmountInReceivingAccountCurrencyColumnValue = isTransactionTransfer(transaction) ?
                ", " + transaction.getAmountInReceivingAccountCurrency() : "";

        return "insert into transactions (time_stamp, transaction_type_id, amount, account_id" +
                optionalReceivingAccountIdColumnName + optionalAmountInReceivingAccountCurrencyColumnName + ") " +
                "values (NOW(), " + transactionTypeId + ", " + transaction.getAmount() + ", " +
                transaction.getAccountId() + optionalReceivingAccountIdColumnValue + optionalAmountInReceivingAccountCurrencyColumnValue + ")";
    }

    public String getTransactionsForAccountQuery(Account account) {
        return "select transactions.id as id," +
                "transactions.time_stamp as time_stamp," +
                "transaction_types.name as transaction_type, " +
                "transactions.amount as amount, " +
                "transactions.amount_in_receiving_account_currency, " +
                "currency_types.name as currency, " +
                "accs.id as account_id, " +
                "accs_users.first_name as account_owner_first_name, " +
                "accs_users.last_name as account_owner_last_name, " +
                "optReceivers.id as optional_receiving_account_id, " +
                "optReceivers_users.first_name as optional_receiving_account_owner_first_name, " +
                "optReceivers_users.last_name as optional_receiving_account_owner_last_name " +
                "from transactions " +
                "join transaction_types on transactions.transaction_type_id = transaction_types.id " +
                "join accounts accs on transactions.account_id = accs.id " +
                "left join accounts optReceivers on transactions.optional_receiving_account_id = optReceivers.id " +
                "join users accs_users on accs.user_id = accs_users.id " +
                "left join users optReceivers_users on optReceivers.user_id = optReceivers_users.id " +
                "join currency_types on accs.currency_type_id = currency_types.id " +
                "where accs.id = " + ((AccountDTO) account).getId() + " or optReceivers.id = " + ((AccountDTO) account).getId();
    }

    public String setCurrencyRate(Integer id, double rateToBaseEUR) {
        return "update currency_types " +
                "set rate_to_base_eur = " + rateToBaseEUR +
                "where id = " + id;
    }

    public String getCurrencyRate(int id) {
        return "select rate_to_base_eur " +
                "from currency_types " +
                "where id = " + id;
    }

    private boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }

    private boolean isTransactionTransfer(Transaction transaction) {
        return transaction.getTransactionType() == TransactionType.TRANSFER;
    }
}
