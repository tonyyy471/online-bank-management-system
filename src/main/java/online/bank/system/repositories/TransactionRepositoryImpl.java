package online.bank.system.repositories;

import online.bank.system.models.Account;
import online.bank.system.models.Transaction;
import online.bank.system.models.utils.Mapper;
import online.bank.system.repositories.utils.ConnectionWrapper;
import online.bank.system.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Service
public class TransactionRepositoryImpl implements TransactionRepository {
    @Override
    public List<Transaction> getTransactionsForAccount(Account account) throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getTransactionsForAccountQuery(account));
        }
        return Mapper.getInstance().resultSetToTransactionsForAccount(resultSet);
    }

    @Override
    public void createTransaction(int transactionTypeId, Transaction transaction) throws SQLException {
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            statement.executeQuery(QueryBuilder.getInstance().createTransactionQuery(transactionTypeId, transaction));
        }
    }
}
