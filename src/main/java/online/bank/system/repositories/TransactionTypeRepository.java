package online.bank.system.repositories;

import online.bank.system.models.TransactionType;

import java.sql.SQLException;
import java.util.List;

public interface TransactionTypeRepository {
    Integer getTransactionTypeIdForName(String name) throws SQLException;

    List<TransactionType> getTransactionTypes() throws SQLException;
}

