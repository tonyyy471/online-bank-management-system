package online.bank.system.repositories;

import online.bank.system.models.CurrencyType;
import online.bank.system.models.utils.Mapper;
import online.bank.system.repositories.utils.ConnectionWrapper;
import online.bank.system.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class CurrencyTypeRepositoryImpl implements CurrencyTypeRepository {
    @Override
    public Integer getCurrencyTypeIdForName(String name) throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getCurrencyTypeIdForNameQuery(name));
        }
        return resultSet.first() ? resultSet.getInt("id") : null;
    }

    @Override
    public List<CurrencyType> getCurrencyTypes() throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getCurrencyTypesQuery());
        }
        return Mapper.getInstance().resultSetToCurrencyTypes(resultSet);
    }

    @Override
    public Double getCurrencyTypeRate(int id) throws SQLException {
        ResultSet resultSet;
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            resultSet = statement.executeQuery(QueryBuilder.getInstance().getCurrencyRate(id));
        }
        return resultSet.first() ? resultSet.getDouble("rate_to_base_eur") : null;
    }

    @Override
    public void setCurrencyTypeRate(Integer id, double rateToBaseEUR) throws SQLException {
        try (Statement statement = ConnectionWrapper.getInstance().getConnection().createStatement()) {
            statement.executeQuery(QueryBuilder.getInstance().setCurrencyRate(id, rateToBaseEUR));
        }
    }
}
