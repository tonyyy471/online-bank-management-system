package online.bank.system.controllers;

import online.bank.system.models.Account;
import online.bank.system.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/bank")
public class AccountRestController {
    private final AccountService accountService;

    @Autowired
    public AccountRestController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/accounts/{userId}")
    public ResponseEntity<List<Account>> getAccountsForUser(@PathVariable int userId) throws SQLException {
        List<Account> accounts = accountService.getAccountsForUser(userId);
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @GetMapping("/account/{accountId}")
    public ResponseEntity<Account> getAccount(@PathVariable int accountId) throws SQLException {
        Account account = accountService.getAccount(accountId);
        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    @PostMapping("/account/create")
    public void createAccount(@RequestBody Account account) throws SQLException {
        accountService.createAccount(account);
    }

    //Tests
    //Security
}
