package online.bank.system.controllers;

import online.bank.system.models.CurrencyType;
import online.bank.system.services.CurrencyTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/bank")
public class CurrencyTypeRestController {
    private final CurrencyTypeService currencyTypeService;

    @Autowired
    public CurrencyTypeRestController(CurrencyTypeService currencyTypeService) {
        this.currencyTypeService = currencyTypeService;
    }

    @GetMapping("/currency-types")
    public ResponseEntity<List<CurrencyType>> getCurrencyTypes() throws SQLException {
        List<CurrencyType> currencyTypes = currencyTypeService.getCurrencyTypes();
        return new ResponseEntity<>(currencyTypes, HttpStatus.OK);
    }

    @PutMapping("/currency-type/set-rate/{currencyType}/{rateToBaseEUR}")
    public void setCurrencyTypeRate(@PathVariable CurrencyType currencyType, @PathVariable double rateToBaseEUR) throws SQLException {
        currencyTypeService.setCurrencyTypeRate(currencyType, rateToBaseEUR);
    }
}
