package online.bank.system.controllers;

import online.bank.system.models.AccountType;
import online.bank.system.services.AccountTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/bank")
public class AccountTypeRestController {
    private final AccountTypeService accountTypeService;

    @Autowired
    public AccountTypeRestController(AccountTypeService accountTypeService) {
        this.accountTypeService = accountTypeService;
    }

    @GetMapping("/account-types")
    public ResponseEntity<List<AccountType>> getAccountTypes() throws SQLException {
        List<AccountType> accountTypes = accountTypeService.getAccountTypes();
        return new ResponseEntity<>(accountTypes, HttpStatus.OK);
    }
}
