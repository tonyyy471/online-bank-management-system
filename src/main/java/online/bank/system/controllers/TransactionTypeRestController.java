package online.bank.system.controllers;

import online.bank.system.models.TransactionType;
import online.bank.system.services.TransactionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/bank")
public class TransactionTypeRestController {
    private final TransactionTypeService transactionTypeService;

    @Autowired
    public TransactionTypeRestController(TransactionTypeService transactionTypeService) {
        this.transactionTypeService = transactionTypeService;
    }

    @GetMapping("/transaction-types")
    public ResponseEntity<List<TransactionType>> getTransactionTypes() throws SQLException {
        List<TransactionType> transactionTypes = transactionTypeService.getTransactionTypes();
        return new ResponseEntity<>(transactionTypes, HttpStatus.OK);
    }
}
