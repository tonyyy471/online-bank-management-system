package online.bank.system.controllers;

import online.bank.system.models.Transaction;
import online.bank.system.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/bank")
public class TransactionRestController {
    private final TransactionService transactionService;

    @Autowired
    public TransactionRestController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping("/transactions/{accountId}")
    public ResponseEntity<List<Transaction>> getTransactionsForAccount(@PathVariable int accountId) throws SQLException {
        List<Transaction> transactions = transactionService.getTransactionsForAccount(accountId);
        return new ResponseEntity<>(transactions, HttpStatus.OK);
    }

    @PostMapping("/create")
    public void createTransaction(@RequestBody Transaction transaction) throws SQLException {
        transactionService.createTransaction(transaction);
    }

    @GetMapping("/transactions/balance/{accountId}")
    public ResponseEntity<Double> getBalanceForAccount(@PathVariable int accountId) throws SQLException {
        double balance = transactionService.getBalanceForAccount(accountId);
        return new ResponseEntity<>(balance, HttpStatus.OK);
    }
}
