package online.bank.system.models;

public enum CurrencyType {
    EUR, USD
}
