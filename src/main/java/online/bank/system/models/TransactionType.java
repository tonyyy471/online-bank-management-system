package online.bank.system.models;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL, TRANSFER
}
