package online.bank.system.models;

public class Account {
    private AccountType accountType;
    private CurrencyType currencyType;
    private Integer userId;

    public Account() {
    }

    public Account(AccountType accountType, CurrencyType currencyType) {
        this.accountType = accountType;
        this.currencyType = currencyType;
    }

    public Account(AccountType accountType, CurrencyType currencyType, Integer userId) {
        this.accountType = accountType;
        this.currencyType = currencyType;
        this.userId = userId;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
