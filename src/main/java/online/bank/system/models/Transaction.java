package online.bank.system.models;

public class Transaction {
    private TransactionType transactionType;
    private Double amount;
    private Integer accountId;
    private Integer optionalReceivingAccountId;
    private Double amountInReceivingAccountCurrency;

    public Transaction() {
    }

    public Transaction(TransactionType transactionType, Double amount, Integer accountId) {
        this.transactionType = transactionType;
        this.amount = amount;
        this.accountId = accountId;
    }

    public Transaction(TransactionType transactionType, Double amount, Integer accountId, Integer optionalReceivingAccountId) {
        this.transactionType = transactionType;
        this.amount = amount;
        this.accountId = accountId;
        this.optionalReceivingAccountId = optionalReceivingAccountId;
    }

    public Transaction(TransactionType transactionType, Double amount, Integer accountId, Integer optionalReceivingAccountId, Double amountInReceivingAccountCurrency) {
        this.transactionType = transactionType;
        this.amount = amount;
        this.accountId = accountId;
        this.optionalReceivingAccountId = optionalReceivingAccountId;
        this.amountInReceivingAccountCurrency = amountInReceivingAccountCurrency;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getOptionalReceivingAccountId() {
        return optionalReceivingAccountId;
    }

    public void setOptionalReceivingAccountId(Integer optionalReceivingAccountId) {
        this.optionalReceivingAccountId = optionalReceivingAccountId;
    }

    public Double getAmountInReceivingAccountCurrency() {
        return amountInReceivingAccountCurrency;
    }

    public void setAmountInReceivingAccountCurrency(Double amountInReceivingAccountCurrency) {
        this.amountInReceivingAccountCurrency = amountInReceivingAccountCurrency;
    }
}
