package online.bank.system.models;

public enum AccountType {
    CHECKING_ACCOUNT, SAVINGS_ACCOUNT,
    MONEY_MARKET_DEPOSIT_ACCOUNT, CERTIFICATE_OF_DEPOSIT_ACCOUNT
}
