package online.bank.system.models.utils;

import online.bank.system.models.*;
import online.bank.system.models.dtos.AccountDTO;
import online.bank.system.models.dtos.TransactionDTO;
import online.bank.system.models.dtos.UserDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Mapper {
    private static Mapper mapper;

    private Mapper() {
    }

    public static Mapper getInstance() {
        if (mapper == null)
            mapper = new Mapper();
        return mapper;
    }

    public User resultSetToUser(ResultSet resultSet, boolean arePassAndPinIncluded) throws SQLException {
        return new UserDTO(resultSet.getInt("id"), resultSet.getString("email"),
                arePassAndPinIncluded ? resultSet.getString("password") : null,
                arePassAndPinIncluded ? resultSet.getString("security_pin") : null,
                resultSet.getString("first_name"), resultSet.getString("last_name"));
    }

    public List<User> resultSetToUsers(ResultSet resultSet) throws SQLException {
        List<User> users = new ArrayList<>();
        while (resultSet.next()) users.add(resultSetToUser(resultSet, false));
        return users;
    }

    public List<AccountType> resultSetToAccountTypes(ResultSet resultSet) throws SQLException {
        List<AccountType> accountTypes = new ArrayList<>();
        while (resultSet.next()) {
            AccountType accountType = AccountType.valueOf(resultSet.getString("name"));
            accountTypes.add(accountType);
        }
        return accountTypes;
    }

    public List<CurrencyType> resultSetToCurrencyTypes(ResultSet resultSet) throws SQLException {
        List<CurrencyType> currencyTypes = new ArrayList<>();
        while (resultSet.next()) {
            CurrencyType currencyType = CurrencyType.valueOf(resultSet.getString("name"));
            currencyTypes.add(currencyType);
        }
        return currencyTypes;
    }

    public List<TransactionType> resultSetToTransactionTypes(ResultSet resultSet) throws SQLException {
        List<TransactionType> transactionTypes = new ArrayList<>();
        while (resultSet.next()) {
            TransactionType transactionType = TransactionType.valueOf(resultSet.getString("name"));
            transactionTypes.add(transactionType);
        }
        return transactionTypes;
    }

    public Account resultSetToAccount(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        AccountType accountType = AccountType.valueOf(resultSet.getString("account_type"));
        CurrencyType currencyType = CurrencyType.valueOf(resultSet.getString("currency_type"));
        return new AccountDTO(id, accountType, currencyType);
    }

    public List<Account> resultSetToAccountsForUser(ResultSet resultSet) throws SQLException {
        List<Account> accounts = new ArrayList<>();
        while (resultSet.next()) accounts.add(resultSetToAccount(resultSet));
        return accounts;
    }

    public List<Transaction> resultSetToTransactionsForAccount(ResultSet resultSet) throws SQLException {
        List<Transaction> transactions = new ArrayList<>();
        while (resultSet.next()) {
            TransactionType transactionType = TransactionType.valueOf(
                    resultSet.getString("transaction_type"));
            double amount = resultSet.getDouble("amount");
            int accountId = resultSet.getInt("account_id");
            int id = resultSet.getInt("id");
            String date = resultSet.getTimestamp("time_stamp").toString();
            String accountOwnerFirstName = resultSet.getString("account_owner_first_name");
            String accountOwnerLastName = resultSet.getString("account_owner_last_name");
            String optionalReceivingAccountOwnerFirstName = resultSet.getString("optional_receiving_account_owner_first_name");
            String optionalReceivingAccountOwnerLastName = resultSet.getString("optional_receiving_account_owner_last_name");
            Integer optionalReceivingAccountId = optionalReceivingAccountOwnerFirstName == null ?
                    null : resultSet.getInt("optional_receiving_account_id");
            Double amountInReceivingAccountCurrency = resultSet.getDouble("amount_in_receiving_account_currency");

            Transaction transaction = new TransactionDTO(transactionType, amount, accountId, optionalReceivingAccountId, id, date,
                    accountOwnerFirstName, accountOwnerLastName, optionalReceivingAccountOwnerFirstName,
                    optionalReceivingAccountOwnerLastName, amountInReceivingAccountCurrency);
            transactions.add(transaction);
        }
        return transactions;
    }
}
