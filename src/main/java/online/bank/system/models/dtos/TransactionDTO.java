package online.bank.system.models.dtos;

import online.bank.system.models.Transaction;
import online.bank.system.models.TransactionType;

public class TransactionDTO extends Transaction {
    private int id;
    private String timeStamp;
    private String accountOwnerFirstName;
    private String accountOwnerLastName;
    private String optionalReceivingAccountFirstName;
    private String optionalReceivingAccountLastName;

    public TransactionDTO(TransactionType transactionType, Double amount, Integer accountId, Integer optionalReceivingAccountId,
                          int id, String timeStamp, String accountOwnerFirstName, String accountOwnerLastName,
                          String optionalReceivingAccountFirstName, String optionalReceivingAccountLastName,
                          Double amountInReceivingAccountCurrency) {
        super(transactionType, amount, accountId, optionalReceivingAccountId, amountInReceivingAccountCurrency);
        this.id = id;
        this.timeStamp = timeStamp;
        this.accountOwnerFirstName = accountOwnerFirstName;
        this.accountOwnerLastName = accountOwnerLastName;
        this.optionalReceivingAccountFirstName = optionalReceivingAccountFirstName;
        this.optionalReceivingAccountLastName = optionalReceivingAccountLastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getAccountOwnerFirstName() {
        return accountOwnerFirstName;
    }

    public void setAccountOwnerFirstName(String accountOwnerFirstName) {
        this.accountOwnerFirstName = accountOwnerFirstName;
    }

    public String getAccountOwnerLastName() {
        return accountOwnerLastName;
    }

    public void setAccountOwnerLastName(String accountOwnerLastName) {
        this.accountOwnerLastName = accountOwnerLastName;
    }

    public String getOptionalReceivingAccountFirstName() {
        return optionalReceivingAccountFirstName;
    }

    public void setOptionalReceivingAccountFirstName(String optionalReceivingAccountFirstName) {
        this.optionalReceivingAccountFirstName = optionalReceivingAccountFirstName;
    }

    public String getOptionalReceivingAccountLastName() {
        return optionalReceivingAccountLastName;
    }

    public void setOptionalReceivingAccountLastName(String optionalReceivingAccountLastName) {
        this.optionalReceivingAccountLastName = optionalReceivingAccountLastName;
    }
}
