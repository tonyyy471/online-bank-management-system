package online.bank.system.models.dtos;

import online.bank.system.models.User;

public class UserDTO extends User {
    private int id;

    public UserDTO(int id, String email, String password, String securityPin, String firstName, String lastName) {
        super(email, password, securityPin, firstName, lastName);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
