package online.bank.system.models.dtos;

import online.bank.system.models.Account;
import online.bank.system.models.AccountType;
import online.bank.system.models.CurrencyType;

public class AccountDTO extends Account {
    private int id;

    public AccountDTO(int id, AccountType accountType, CurrencyType currencyType) {
        super(accountType, currencyType);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
