package online.bank.system.services;

import online.bank.system.models.AccountType;

import java.sql.SQLException;
import java.util.List;

public interface AccountTypeService {
    Integer getAccountTypeIdForName(String name) throws SQLException;

    List<AccountType> getAccountTypes() throws SQLException;
}
