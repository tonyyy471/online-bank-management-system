package online.bank.system.services;

import online.bank.system.models.TransactionType;

import java.sql.SQLException;
import java.util.List;

public interface TransactionTypeService {
    Integer getTransactionTypeIdForName(String name) throws SQLException;

    List<TransactionType> getTransactionTypes() throws SQLException;
}
