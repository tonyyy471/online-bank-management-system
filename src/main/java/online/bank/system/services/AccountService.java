package online.bank.system.services;

import online.bank.system.models.Account;

import java.sql.SQLException;
import java.util.List;

public interface AccountService {
    List<Account> getAccountsForUser(int userId) throws SQLException;

    Account getAccount(int accountId) throws SQLException;

    void createAccount(Account account) throws SQLException;
}
