package online.bank.system.services;

import online.bank.system.models.TransactionType;
import online.bank.system.repositories.TransactionTypeRepository;
import online.bank.system.services.utils.TransactionTypeVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class TransactionTypeServiceImpl implements TransactionTypeService{
    private final TransactionTypeRepository transactionTypeRepository;

    @Autowired
    public TransactionTypeServiceImpl(TransactionTypeRepository transactionTypeRepository) {
        this.transactionTypeRepository = transactionTypeRepository;
    }

    @Override
    public Integer getTransactionTypeIdForName(String name) throws SQLException {
        Integer id = transactionTypeRepository.getTransactionTypeIdForName(name);
        TransactionTypeVerifier.getInstance().verifyId(id, name);
        return id;
    }

    @Override
    public List<TransactionType> getTransactionTypes() throws SQLException {
        List<TransactionType> transactionTypes = transactionTypeRepository.getTransactionTypes();
        TransactionTypeVerifier.getInstance().verifySystemHasTransactionTypes(transactionTypes);
        return transactionTypes;
    }
}
