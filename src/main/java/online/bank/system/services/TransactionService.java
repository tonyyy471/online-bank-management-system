package online.bank.system.services;

import online.bank.system.models.Transaction;

import java.sql.SQLException;
import java.util.List;

public interface TransactionService {
    List<Transaction> getTransactionsForAccount(int id) throws SQLException;

    void createTransaction(Transaction transaction) throws SQLException;

    double getBalanceForAccount(int id) throws SQLException;
}
