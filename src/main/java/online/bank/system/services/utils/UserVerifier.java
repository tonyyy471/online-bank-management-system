package online.bank.system.services.utils;

import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotAcceptableException;
import online.bank.system.exceptions.NotExistingException;
import online.bank.system.models.User;

import java.util.List;

public class UserVerifier {
    private static UserVerifier userVerifier;

    private UserVerifier() {
    }

    public static UserVerifier getInstance() {
        if (userVerifier == null)
            userVerifier = new UserVerifier();
        return userVerifier;
    }

    public void verifyFieldsAreNotNull(User user) {
        verify(user.getEmail(), "Email");
        verify(user.getPassword(), "Password");
        verify(user.getSecurityPin(), "Security pin");
        verify(user.getFirstName(), "First name");
        verify(user.getLastName(), "Last name");
    }

    public void verifyUserDoesNotExist(User user) {
        if (user != null) throw new NotAcceptableException("User with email " + user.getEmail() + " exists");
    }

    public void verifyUserDoesExist(User user, String email) {
        if (user == null) throw new NotExistingException("User with email " + email + " does not exist");
    }

    public void verifyUserDoesExist(User user, int id) {
        if (user == null) throw new NotExistingException("User with id " + id + " does not exist");
    }

    public void verifySystemHasUsers(List<User> users) {
        if (users.isEmpty()) throw new NoContentException("There are no users in the system");
    }

    private void verify(String field, String fieldName) {
        if (field == null || field.length() == 0) throw new NotAcceptableException(fieldName + " is required");
    }
}
