package online.bank.system.services.utils;

import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotExistingException;
import online.bank.system.models.TransactionType;

import java.util.List;

public class TransactionTypeVerifier {
    private static TransactionTypeVerifier transactionTypeVerifier;

    private TransactionTypeVerifier() {
    }

    public static TransactionTypeVerifier getInstance() {
        if (transactionTypeVerifier == null)
            transactionTypeVerifier = new TransactionTypeVerifier();
        return transactionTypeVerifier;
    }

    public void verifySystemHasTransactionTypes(List<TransactionType> transactionTypes) {
        if (transactionTypes.isEmpty())
            throw new NoContentException("There are no transaction types in the system");
    }

    public void verifyId(Integer id, String name) {
        if (id == null) throw new NotExistingException("Transaction type with name " + name + " does not exist");
    }
}
