package online.bank.system.services.utils;

import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotExistingException;
import online.bank.system.models.AccountType;

import java.util.List;

public class AccountTypeVerifier {
    private static AccountTypeVerifier AccountTypeVerifier;

    private AccountTypeVerifier() {
    }

    public static AccountTypeVerifier getInstance() {
        if (AccountTypeVerifier == null)
            AccountTypeVerifier = new AccountTypeVerifier();
        return AccountTypeVerifier;
    }

    public void verifySystemHasAccountTypes(List<AccountType> accountTypes) {
        if (accountTypes.isEmpty()) throw new NoContentException("There are no account types in the system");
    }

    public void verifyId(Integer id, String name) {
        if (id == null) throw new NotExistingException("Account type with name " + name + " does not exist");
    }
}
