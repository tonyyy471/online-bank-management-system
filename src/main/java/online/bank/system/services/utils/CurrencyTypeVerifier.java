package online.bank.system.services.utils;

import online.bank.system.exceptions.NotAcceptableException;
import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotExistingException;
import online.bank.system.models.CurrencyType;

import java.util.List;

public class CurrencyTypeVerifier {
    private static CurrencyTypeVerifier currencyTypeVerifier;

    private CurrencyTypeVerifier() {
    }

    public static CurrencyTypeVerifier getInstance() {
        if (currencyTypeVerifier == null)
            currencyTypeVerifier = new CurrencyTypeVerifier();
        return currencyTypeVerifier;
    }


    public void verifySystemHasCurrencyTypes(List<CurrencyType> currencyTypes) {
        if (currencyTypes.isEmpty()) throw new NoContentException("There are no currency types in the system");
    }

    public void verifyCurrencyTypeExists(Integer id, String name) {
        if (id == null) throw new NotExistingException("Currency type with name " + name + " does not exist");
    }

    public void verifyCurrencyTypeIsNotEUR(String name) {
        if (name.equals("EUR")) throw new NotAcceptableException("EUR is the base currency and it's rate is always 1");
    }

    public void verifyCurrencyTypeRate(String name, Double rate) {
        if (rate == null) throw new NotExistingException("Rate for currency with name " + name + " does not exist");
    }
}
