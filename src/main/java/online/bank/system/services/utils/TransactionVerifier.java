package online.bank.system.services.utils;

import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotAcceptableException;
import online.bank.system.models.Transaction;
import online.bank.system.models.TransactionType;

import java.util.List;

public class TransactionVerifier {
    private static TransactionVerifier transactionVerifier;

    private TransactionVerifier() {
    }

    public static TransactionVerifier getInstance() {
        if (transactionVerifier == null)
            transactionVerifier = new TransactionVerifier();
        return transactionVerifier;
    }

    public void verifyFieldsAreNotNull(Transaction transaction) {
        if (transaction.getTransactionType() == null) throw new NotAcceptableException("Transaction type is required");
        if (transaction.getAmount() == null) throw new NotAcceptableException("Amount is required");
        if (transaction.getAccountId() == null) throw new NotAcceptableException("Account id is required");
        if (isTransactionTypeTransfer(transaction)) {
            if (transaction.getOptionalReceivingAccountId() == null)
                throw new NotAcceptableException("Receiving account id is required");
        }
    }

    public void verifyReceivingAccountIdIsNotSelf(Transaction transaction) {
        if (transaction.getAccountId().equals(transaction.getOptionalReceivingAccountId()))
            throw new NotAcceptableException("Account id and receiving account id cannot be the same");
    }

    public void verifyAccountHasTransactions(List<Transaction> transactions, int id) {
        if (transactions.isEmpty()) throw new NoContentException("There are no transactions for account with id " + id);
    }

    public void verifyAccountHasEnoughBalance(double balance, Double amount) {
        if (balance < amount) throw new NotAcceptableException("Not enough funds to complete the transaction");
    }

    private boolean isTransactionTypeTransfer(Transaction transaction) {
        return transaction.getTransactionType() == TransactionType.TRANSFER;
    }
}
