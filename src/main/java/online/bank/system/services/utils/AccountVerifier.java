package online.bank.system.services.utils;

import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotAcceptableException;
import online.bank.system.exceptions.NotExistingException;
import online.bank.system.models.Account;
import online.bank.system.models.User;
import online.bank.system.models.dtos.UserDTO;

import java.util.List;

public class AccountVerifier {
    private static AccountVerifier accountVerifier;

    private AccountVerifier() {
    }

    public static AccountVerifier getInstance() {
        if (accountVerifier == null)
            accountVerifier = new AccountVerifier();
        return accountVerifier;
    }

    public void verifyFieldsAreNotNull(Account account) {
        if (account.getAccountType() == null) throw new NotAcceptableException("Account type is required");
        if (account.getCurrencyType() == null) throw new NotAcceptableException("Currency type is required");
    }

    public void verifyAccountDoesExist(Account account, int id) {
        if (account == null) throw new NotExistingException("Account with id " + id + " does not exist");
    }

    public void verifyUserHasAccounts(List<Account> accounts, User user) {
        if (accounts.isEmpty()) throw new NoContentException("There are no accounts for user with id " + ((UserDTO)user).getId());
    }
}
