package online.bank.system.services;

import online.bank.system.models.CurrencyType;

import java.sql.SQLException;
import java.util.List;

public interface CurrencyTypeService {
    Integer getCurrencyTypeIdForName(String name) throws SQLException;

    List<CurrencyType> getCurrencyTypes() throws SQLException;

    Double getCurrencyTypeRate(CurrencyType currencyType) throws SQLException;

    void setCurrencyTypeRate(CurrencyType currencyType, double rateToBaseEUR) throws SQLException;
}
