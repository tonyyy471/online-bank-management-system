package online.bank.system.services;

import online.bank.system.models.CurrencyType;
import online.bank.system.repositories.CurrencyTypeRepository;
import online.bank.system.services.utils.CurrencyTypeVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class CurrencyTypeServiceImpl implements CurrencyTypeService {
    private final CurrencyTypeRepository currencyTypeRepository;

    @Autowired
    public CurrencyTypeServiceImpl(CurrencyTypeRepository currencyTypeRepository) {
        this.currencyTypeRepository = currencyTypeRepository;
    }

    @Override
    public Integer getCurrencyTypeIdForName(String name) throws SQLException {
        Integer id = currencyTypeRepository.getCurrencyTypeIdForName(name);
        CurrencyTypeVerifier.getInstance().verifyCurrencyTypeExists(id, name);
        return id;
    }

    @Override
    public List<CurrencyType> getCurrencyTypes() throws SQLException {
        List<CurrencyType> currencyTypes = currencyTypeRepository.getCurrencyTypes();
        CurrencyTypeVerifier.getInstance().verifySystemHasCurrencyTypes(currencyTypes);
        return currencyTypes;
    }

    @Override
    public Double getCurrencyTypeRate(CurrencyType currencyType) throws SQLException {
        int id = getCurrencyTypeIdForName(currencyType.name());
        Double rate = currencyTypeRepository.getCurrencyTypeRate(id);
        CurrencyTypeVerifier.getInstance().verifyCurrencyTypeRate(currencyType.name(), rate);
        return rate;
    }

    @Override
    public void setCurrencyTypeRate(CurrencyType currencyType, double rateToBaseEUR) throws SQLException {
        Integer id = getCurrencyTypeIdForName(currencyType.name());
        CurrencyTypeVerifier.getInstance().verifyCurrencyTypeIsNotEUR(currencyType.name());
        currencyTypeRepository.setCurrencyTypeRate(id, rateToBaseEUR);
    }
}
