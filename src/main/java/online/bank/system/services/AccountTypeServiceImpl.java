package online.bank.system.services;

import online.bank.system.models.AccountType;
import online.bank.system.repositories.AccountTypeRepository;
import online.bank.system.services.utils.AccountTypeVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class AccountTypeServiceImpl implements AccountTypeService {
    private final AccountTypeRepository accountTypeRepository;

    @Autowired
    public AccountTypeServiceImpl(AccountTypeRepository accountTypeRepository) {
        this.accountTypeRepository = accountTypeRepository;
    }

    @Override
    public Integer getAccountTypeIdForName(String name) throws SQLException {
        Integer id = accountTypeRepository.getAccountTypeIdForName(name);
        AccountTypeVerifier.getInstance().verifyId(id, name);
        return id;
    }

    @Override
    public List<AccountType> getAccountTypes() throws SQLException {
        List<AccountType> accountTypes = accountTypeRepository.getAccountTypes();
        AccountTypeVerifier.getInstance().verifySystemHasAccountTypes(accountTypes);
        return accountTypes;
    }
}
