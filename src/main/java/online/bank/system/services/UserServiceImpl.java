package online.bank.system.services;

import online.bank.system.models.User;
import online.bank.system.repositories.UserRepository;
import online.bank.system.services.utils.UserVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getUsers() throws SQLException {
        List<User> users = userRepository.getUsers();
        UserVerifier.getInstance().verifySystemHasUsers(users);
        return users;
    }

    @Override
    public User getUser(String email) throws SQLException {
        User user = userRepository.getUser(email);
        UserVerifier.getInstance().verifyUserDoesExist(user, email);
        return user;
    }

    @Override
    public User getUser(int id) throws SQLException {
        User user = userRepository.getUser(id);
        UserVerifier.getInstance().verifyUserDoesExist(user, id);
        return user;
    }

    @Override
    public void createUser(User user) throws SQLException {
        UserVerifier.getInstance().verifyFieldsAreNotNull(user);
        UserVerifier.getInstance().verifyUserDoesNotExist(userRepository.getUser(user.getEmail()));
        userRepository.createUser(user);
    }

    @Override
    public void updateUser(User user, int id) throws SQLException {
        UserVerifier.getInstance().verifyUserDoesExist(userRepository.getUser(id), id);
        UserVerifier.getInstance().verifyUserDoesNotExist(userRepository.getUser(user.getEmail()));
        userRepository.updateUser(user, id);
    }
}
