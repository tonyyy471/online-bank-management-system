package online.bank.system.services;

import online.bank.system.models.Account;
import online.bank.system.models.User;
import online.bank.system.repositories.AccountRepository;
import online.bank.system.services.utils.AccountVerifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final UserService userService;
    private final AccountTypeService accountTypeService;
    private final CurrencyTypeService currencyTypeService;

    public AccountServiceImpl(AccountRepository accountRepository, UserService userService,
                              AccountTypeService accountTypeService, CurrencyTypeService currencyTypeService) {
        this.accountRepository = accountRepository;
        this.userService = userService;
        this.accountTypeService = accountTypeService;
        this.currencyTypeService = currencyTypeService;
    }

    @Override
    public List<Account> getAccountsForUser(int userId) throws SQLException {
        User user = userService.getUser(userId);
        List<Account> accounts = accountRepository.getAccountsForUser(userId);
        AccountVerifier.getInstance().verifyUserHasAccounts(accounts, user);
        return accounts;
    }

    @Override
    public Account getAccount(int accountId) throws SQLException {
        Account account = accountRepository.getAccount(accountId);
        AccountVerifier.getInstance().verifyAccountDoesExist(account, accountId);
        return account;
    }

    @Override
    public void createAccount(Account account) throws SQLException {
        AccountVerifier.getInstance().verifyFieldsAreNotNull(account);

        User user = userService.getUser(account.getUserId());
        int accountTypeId = accountTypeService.getAccountTypeIdForName(account.getAccountType().name());
        int currencyTypeId = currencyTypeService.getCurrencyTypeIdForName(account.getCurrencyType().name());

        accountRepository.createAccount(accountTypeId, currencyTypeId, user);
    }
}
