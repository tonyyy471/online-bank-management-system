package online.bank.system.services;

import online.bank.system.models.User;

import java.sql.SQLException;
import java.util.List;

public interface UserService {
    List<User> getUsers() throws SQLException;

    User getUser(String email) throws SQLException;

    User getUser(int id) throws SQLException;

    void createUser(User user) throws SQLException;

    void updateUser(User user, int id) throws SQLException;
}
