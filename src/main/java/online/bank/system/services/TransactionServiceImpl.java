package online.bank.system.services;

import online.bank.system.models.Account;
import online.bank.system.models.Transaction;
import online.bank.system.models.TransactionType;
import online.bank.system.models.dtos.AccountDTO;
import online.bank.system.repositories.TransactionRepository;
import online.bank.system.services.utils.TransactionVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionRepository;
    private final AccountService accountService;
    private final TransactionTypeService transactionTypeService;
    private final CurrencyTypeService currencyTypeService;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, AccountService accountService,
                                  TransactionTypeService transactionTypeService, CurrencyTypeService currencyTypeService) {
        this.transactionRepository = transactionRepository;
        this.accountService = accountService;
        this.transactionTypeService = transactionTypeService;
        this.currencyTypeService = currencyTypeService;
    }

    @Override
    public List<Transaction> getTransactionsForAccount(int id) throws SQLException {
        Account account = accountService.getAccount(id);
        List<Transaction> transactions = transactionRepository.getTransactionsForAccount(account);
        TransactionVerifier.getInstance().verifyAccountHasTransactions(transactions, id);
        return transactions;
    }

    @Override
    public void createTransaction(Transaction transaction) throws SQLException {
        TransactionVerifier.getInstance().verifyFieldsAreNotNull(transaction);

        Account account = accountService.getAccount(transaction.getAccountId());
        Account optionalReceivingAccount = isTransfer(transaction) ?
                accountService.getAccount(transaction.getOptionalReceivingAccountId()) : null;

        if (isTransfer(transaction))
            TransactionVerifier.getInstance().verifyReceivingAccountIdIsNotSelf(transaction);

        if (isWithdrawal(transaction) || isTransfer(transaction)) {
            double balance = getBalanceForAccount(((AccountDTO) account).getId());
            TransactionVerifier.getInstance().verifyAccountHasEnoughBalance(balance, transaction.getAmount());
        }

        if (isTransfer(transaction) && areAccountsCurrenciesDifferent(account, optionalReceivingAccount)) {
            double convertedAmount =
                    convertAmountToReceivingAccountCurrency(transaction.getAmount(), account, optionalReceivingAccount);
            transaction.setAmountInReceivingAccountCurrency(convertedAmount);
        }

        int transactionTypeId = transactionTypeService.getTransactionTypeIdForName(transaction.getTransactionType().name());

        transactionRepository.createTransaction(transactionTypeId, transaction);
    }

    @Override
    public double getBalanceForAccount(int id) throws SQLException {
        double balance = 0;
        List<Transaction> transactions = getTransactionsForAccount(id);

        for (Transaction transaction : transactions) {
            if (isDeposit(transaction)) balance += transaction.getAmount();
            else if (isWithdrawal(transaction)) balance -= transaction.getAmount();
            else {
                if (isTransferReceiver(id, transaction)) balance += transaction.getAmountInReceivingAccountCurrency();
                else balance -= transaction.getAmount();
            }
        }
        return balance;
    }

    private double convertAmountToReceivingAccountCurrency(double amount, Account account, Account optionalReceivingAccount) throws SQLException {
        double senderCurrencyRateToEUR = currencyTypeService.getCurrencyTypeRate(account.getCurrencyType());
        double receiverCurrencyRateToEUR = currencyTypeService.getCurrencyTypeRate(optionalReceivingAccount.getCurrencyType());
        return amount * (senderCurrencyRateToEUR / receiverCurrencyRateToEUR);
    }

    private boolean areAccountsCurrenciesDifferent(Account account, Account optionalReceivingAccount) {
        return account.getCurrencyType() != optionalReceivingAccount.getCurrencyType();
    }

    private boolean isTransfer(Transaction transaction) {
        return transaction.getTransactionType() == TransactionType.TRANSFER;
    }

    private boolean isDeposit(Transaction transaction) {
        return transaction.getTransactionType() == TransactionType.DEPOSIT;
    }

    private boolean isWithdrawal(Transaction transaction) {
        return transaction.getTransactionType() == TransactionType.WITHDRAWAL;
    }

    private boolean isTransferReceiver(int id, Transaction transaction) {
        return isTransfer(transaction) && id == transaction.getOptionalReceivingAccountId();
    }
}
