package online.bank.system;

import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotExistingException;
import online.bank.system.models.TransactionType;
import online.bank.system.repositories.TransactionTypeRepositoryImpl;
import online.bank.system.services.TransactionTypeServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TransactionTypeServiceImplTests {
    @Mock
    TransactionTypeRepositoryImpl transactionTypeRepository;
    @InjectMocks
    TransactionTypeServiceImpl transactionTypeService;

    @Test
    public void getTransactionTypeIdForName_Should_ReturnId_When_NameIsValid() throws SQLException {
        Mockito.when(transactionTypeRepository.getTransactionTypeIdForName("DEPOSIT")).thenReturn(1);

        int id = transactionTypeService.getTransactionTypeIdForName("DEPOSIT");

        Assert.assertEquals(1, id);
    }

    @Test(expected = NotExistingException.class)
    public void getTransactionTypeIdForName_Should_ThrowException_When_NameIsInvalid() throws SQLException {
        Mockito.when(transactionTypeRepository.getTransactionTypeIdForName("RANDOM_INVALID_NAME")).thenReturn(null);

        int id = transactionTypeService.getTransactionTypeIdForName("RANDOM_INVALID_NAME");

        Assert.assertEquals(1, id);
    }

    @Test
    public void getTransactionTypes_Should_ReturnTransactionTypes_When_TransactionTypesExist() throws SQLException {
        List<TransactionType> mockTransactionTypes = Arrays.asList(TransactionType.DEPOSIT,
                TransactionType.WITHDRAWAL, TransactionType.TRANSFER);

        Mockito.when(transactionTypeRepository.getTransactionTypes()).thenReturn(mockTransactionTypes);

        List<TransactionType> transactionTypes = transactionTypeService.getTransactionTypes();

        Assert.assertEquals(transactionTypes, mockTransactionTypes);
    }

    @Test(expected = NoContentException.class)
    public void getTransactionTypes_Should_ThrowException_When_TransactionTypesDontExist() throws SQLException {
        List<TransactionType> mockTransactionTypes = new ArrayList<>();

        Mockito.when(transactionTypeRepository.getTransactionTypes()).thenReturn(mockTransactionTypes);

        List<TransactionType> transactionTypes = transactionTypeService.getTransactionTypes();

        Assert.assertEquals(transactionTypes, mockTransactionTypes);
    }
}
