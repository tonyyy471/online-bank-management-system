package online.bank.system;

import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotAcceptableException;
import online.bank.system.exceptions.NotExistingException;
import online.bank.system.models.CurrencyType;
import online.bank.system.repositories.CurrencyTypeRepositoryImpl;
import online.bank.system.services.CurrencyTypeServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyTypeServiceTests {
    @Mock
    CurrencyTypeRepositoryImpl currencyTypeRepository;
    @InjectMocks
    CurrencyTypeServiceImpl currencyTypeService;

    @Test
    public void getCurrencyTypeIdForName_Should_ReturnId_When_NameIsValid() throws SQLException {
        Mockito.when(currencyTypeRepository.getCurrencyTypeIdForName("EUR")).thenReturn(1);

        int id = currencyTypeService.getCurrencyTypeIdForName("EUR");

        Assert.assertEquals(1, id);
    }

    @Test(expected = NotExistingException.class)
    public void getCurrencyTypeIdForName_Should_ThrowException_When_NameIsInvalid() throws SQLException {
        Mockito.when(currencyTypeRepository.getCurrencyTypeIdForName("RANDOM_INVALID_NAME")).thenReturn(null);

        int id = currencyTypeService.getCurrencyTypeIdForName("RANDOM_INVALID_NAME");

        Assert.assertEquals(1, id);
    }

    @Test
    public void getCurrencyTypes_Should_ReturnCurrencyTypes_When_CurrencyTypesExist() throws SQLException {
        List<CurrencyType> mockCurrencyTypes = Arrays.asList(CurrencyType.EUR, CurrencyType.USD);

        Mockito.when(currencyTypeRepository.getCurrencyTypes()).thenReturn(mockCurrencyTypes);

        List<CurrencyType> currencyTypes = currencyTypeService.getCurrencyTypes();

        Assert.assertEquals(currencyTypes, mockCurrencyTypes);
    }

    @Test(expected = NoContentException.class)
    public void getCurrencyTypes_Should_ThrowException_When_CurrencyTypesDontExist() throws SQLException {
        List<CurrencyType> mockCurrencyTypes = new ArrayList<>();

        Mockito.when(currencyTypeRepository.getCurrencyTypes()).thenReturn(mockCurrencyTypes);

        List<CurrencyType> currencyTypes = currencyTypeService.getCurrencyTypes();

        Assert.assertEquals(currencyTypes, mockCurrencyTypes);
    }

    @Test
    public void getCurrencyTypeRate_Should_ReturnCurrencyTypeRate_When_CurrencyTypeRateExists() throws SQLException {
        Mockito.when(currencyTypeService.getCurrencyTypeIdForName("USD")).thenReturn(2);
        Mockito.when(currencyTypeRepository.getCurrencyTypeRate(2)).thenReturn(0.81);

        double currencyTypeRate = currencyTypeService.getCurrencyTypeRate(CurrencyType.USD);

        Assert.assertEquals(0.81, currencyTypeRate, 0);
    }

    @Test(expected = NotExistingException.class)
    public void getCurrencyTypeRate_Should_ThrowException_When_CurrencyTypeRateDoesntExist() throws SQLException {
        Mockito.when(currencyTypeService.getCurrencyTypeIdForName("USD")).thenReturn(2);
        Mockito.when(currencyTypeRepository.getCurrencyTypeRate(2)).thenReturn(null);

        double currencyTypeRate = currencyTypeService.getCurrencyTypeRate(CurrencyType.USD);

        Assert.assertEquals(0.81, currencyTypeRate, 0);
    }

    @Test
    public void setCurrencyTypeRate_Should_Call_RepositorySetCurrencyTypeRate_When_CurrencyTypeIsNotEUR() throws SQLException {
        Mockito.when(currencyTypeService.getCurrencyTypeIdForName("USD")).thenReturn(2);

        currencyTypeService.setCurrencyTypeRate(CurrencyType.USD, 0.82);

        Mockito.verify(currencyTypeRepository, Mockito.times(1)).setCurrencyTypeRate(2, 0.82);
    }

    @Test(expected = NotAcceptableException.class)
    public void setCurrencyTypeRate_Should_ThrowException_When_CurrencyTypeIsEUR() throws SQLException {
        Mockito.when(currencyTypeService.getCurrencyTypeIdForName("EUR")).thenReturn(1);

        currencyTypeService.setCurrencyTypeRate(CurrencyType.EUR, 1.5);

        Mockito.verify(currencyTypeRepository, Mockito.never()).setCurrencyTypeRate(1, 1.5);
    }
}
