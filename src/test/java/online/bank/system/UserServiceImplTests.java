package online.bank.system;

import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotAcceptableException;
import online.bank.system.exceptions.NotExistingException;
import online.bank.system.models.User;
import online.bank.system.models.dtos.UserDTO;
import online.bank.system.repositories.UserRepositoryImpl;
import online.bank.system.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {
    @Mock
    UserRepositoryImpl userRepository;
    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void getUsers_Should_ReturnUsers_When_UsersExist() throws SQLException {
        List<User> mockUsers = Arrays.asList(
                new UserDTO(1, "email@gmail.com", "1234", "1234", "Ivan", "Ivanov"),
                new UserDTO(2, "emailOne@gmail.com", "5678", "5678", "Georgi", "Ivanov"),
                new UserDTO(3, "emailTwo@gmail.com", "0000", "0000", "Ivan", "Georgiev"));

        Mockito.when(userRepository.getUsers()).thenReturn(mockUsers);

        List<User> users = userService.getUsers();

        Assert.assertEquals(users, mockUsers);
    }

    @Test(expected = NoContentException.class)
    public void getUsers_Should_ThrowException_When_UsersDontExist() throws SQLException {
        List<User> mockUsers = new ArrayList<>();

        Mockito.when(userRepository.getUsers()).thenReturn(mockUsers);

        List<User> users = userService.getUsers();

        Assert.assertEquals(users, mockUsers);
    }

    @Test
    public void getUserById_Should_ReturnUser_When_UserExists() throws SQLException {
        User mockUser = new UserDTO(1, "email@gmail.com", "1234", "1234", "Ivan", "Ivanov");

        Mockito.when(userRepository.getUser(1)).thenReturn(mockUser);

        User user = userService.getUser(1);

        Assert.assertEquals(((UserDTO) user).getId(), ((UserDTO) mockUser).getId());
    }

    @Test(expected = NotExistingException.class)
    public void getUserById_Should_ThrowException_When_UserDoesntExist() throws SQLException {
        User mockUser = new UserDTO(1, "email@gmail.com", "1234", "1234", "Ivan", "Ivanov");

        Mockito.when(userRepository.getUser(1)).thenReturn(null);

        User user = userService.getUser(1);

        Assert.assertEquals(((UserDTO) user).getId(), ((UserDTO) mockUser).getId());
    }

    @Test
    public void getUserByEmail_Should_ReturnUser_When_UserExists() throws SQLException {
        User mockUser = new UserDTO(1, "email@gmail.com", "1234", "1234", "Ivan", "Ivanov");

        Mockito.when(userRepository.getUser("email@gmail.com")).thenReturn(mockUser);

        User user = userService.getUser("email@gmail.com");

        Assert.assertEquals(user.getEmail(), mockUser.getEmail());
    }

    @Test(expected = NotExistingException.class)
    public void getUserByEmail_Should_ThrowException_When_UserDoesntExist() throws SQLException {
        User mockUser = new UserDTO(1, "email@gmail.com", "1234", "1234", "Ivan", "Ivanov");

        Mockito.when(userRepository.getUser("email@gmail.com")).thenReturn(null);

        User user = userService.getUser("email@gmail.com");

        Assert.assertEquals(user.getEmail(), mockUser.getEmail());
    }

    @Test
    public void createUser_Should_Call_RepositoryCreateUser_When_EmailDoesntExist_And_FieldsAreNotNull() throws SQLException {
        User user = new User("email@gmail.com", "1234", "1234", "Ivan", "Ivanov");

        userService.createUser(user);

        Mockito.verify(userRepository, Mockito.times(1)).createUser(user);
    }

    @Test(expected = NotAcceptableException.class)
    public void createUser_Should_ThrowException_When_FieldsAreNull() throws SQLException {
        User user = new User(null, null, null, null, null);

        userService.createUser(user);

        Mockito.verify(userRepository, Mockito.never()).createUser(user);
    }

    @Test(expected = NotAcceptableException.class)
    public void createUser_Should_ThrowException_When_EmailExists() throws SQLException {
        User user = new User("email@gmail.com", "1234", "1234", "Ivan", "Ivanov");

        Mockito.when(userRepository.getUser("email@gmail.com")).thenReturn(user);

        userService.createUser(user);

        Mockito.verify(userRepository, Mockito.never()).createUser(user);
    }

    @Test
    public void updateUser_Should_Call_RepositoryUpdateUser_When_UserExists_And_NewEmailDoesntExist() throws SQLException {
        User user = new User("email@gmail.com", "1234", "1234", "Ivan", "Ivanov");

        Mockito.when(userRepository.getUser(1)).thenReturn(user);
        Mockito.when(userRepository.getUser("email@gmail.com")).thenReturn(null);

        userService.updateUser(user, 1);

        Mockito.verify(userRepository, Mockito.times(1)).updateUser(user, 1);
    }

    @Test(expected = NotExistingException.class)
    public void updateUser_Should_ThrowException_When_UserDoesntExist() throws SQLException {
        User user = new User("email@gmail.com", "1234", "1234", "Ivan", "Ivanov");

        Mockito.when(userRepository.getUser(1)).thenReturn(null);

        userService.updateUser(user, 1);

        Mockito.verify(userRepository, Mockito.never()).updateUser(user, 1);
    }

    @Test(expected = NotAcceptableException.class)
    public void updateUser_Should_ThrowException_When_NewEmailExists() throws SQLException {
        User user = new User("email@gmail.com", "1234", "1234", "Ivan", "Ivanov");

        Mockito.when(userRepository.getUser(1)).thenReturn(user);
        Mockito.when(userRepository.getUser("email@gmail.com")).thenReturn(user);

        userService.updateUser(user, 1);

        Mockito.verify(userRepository, Mockito.never()).updateUser(user, 1);
    }
}
