package online.bank.system;

import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotExistingException;
import online.bank.system.models.AccountType;
import online.bank.system.repositories.AccountTypeRepositoryImpl;
import online.bank.system.services.AccountTypeServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AccountTypeServiceImplTests {
    @Mock
    AccountTypeRepositoryImpl accountTypeRepository;
    @InjectMocks
    AccountTypeServiceImpl accountTypeService;

    @Test
    public void getAccountTypeIdForName_Should_ReturnId_When_NameIsValid() throws SQLException {
        Mockito.when(accountTypeRepository.getAccountTypeIdForName("CHECKING_ACCOUNT")).thenReturn(1);

        int id = accountTypeService.getAccountTypeIdForName("CHECKING_ACCOUNT");

        Assert.assertEquals(1, id);
    }

    @Test(expected = NotExistingException.class)
    public void getAccountTypeIdForName_Should_ThrowException_When_NameIsInvalid() throws SQLException {
        Mockito.when(accountTypeRepository.getAccountTypeIdForName("RANDOM_INVALID_NAME")).thenReturn(null);

        int id = accountTypeService.getAccountTypeIdForName("RANDOM_INVALID_NAME");

        Assert.assertEquals(1, id);
    }

    @Test
    public void getAccountTypes_Should_ReturnAccountTypes_When_AccountTypesExist() throws SQLException {
        List<AccountType> mockAccountTypes = Arrays.asList(AccountType.CHECKING_ACCOUNT, AccountType.SAVINGS_ACCOUNT,
                AccountType.MONEY_MARKET_DEPOSIT_ACCOUNT, AccountType.CERTIFICATE_OF_DEPOSIT_ACCOUNT);

        Mockito.when(accountTypeRepository.getAccountTypes()).thenReturn(mockAccountTypes);

        List<AccountType> accountTypes = accountTypeService.getAccountTypes();

        Assert.assertEquals(accountTypes, mockAccountTypes);
    }

    @Test(expected = NoContentException.class)
    public void getAccountTypes_Should_ThrowException_When_AccountTypesDontExist() throws SQLException {
        List<AccountType> mockAccountTypes = new ArrayList<>();

        Mockito.when(accountTypeRepository.getAccountTypes()).thenReturn(mockAccountTypes);

        List<AccountType> accountTypes = accountTypeService.getAccountTypes();

        Assert.assertEquals(accountTypes, mockAccountTypes);
    }
}
