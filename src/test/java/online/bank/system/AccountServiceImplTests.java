package online.bank.system;

import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotAcceptableException;
import online.bank.system.exceptions.NotExistingException;
import online.bank.system.models.Account;
import online.bank.system.models.AccountType;
import online.bank.system.models.CurrencyType;
import online.bank.system.models.User;
import online.bank.system.models.dtos.AccountDTO;
import online.bank.system.models.dtos.UserDTO;
import online.bank.system.repositories.AccountRepositoryImpl;
import online.bank.system.services.AccountServiceImpl;
import online.bank.system.services.AccountTypeServiceImpl;
import online.bank.system.services.CurrencyTypeServiceImpl;
import online.bank.system.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTests {
    @Mock
    AccountRepositoryImpl accountRepository;
    @Mock
    UserServiceImpl userService;
    @Mock
    AccountTypeServiceImpl accountTypeService;
    @Mock
    CurrencyTypeServiceImpl currencyTypeService;
    @InjectMocks
    AccountServiceImpl accountService;

    @Test
    public void getAccountsForUser_Should_ReturnAccountsForUser_When_UserHasAccounts() throws SQLException {
        User mockUser = new UserDTO(1, "email@gmail.com", "1234", "1234", "Ivan", "Ivanov");
        Mockito.when(userService.getUser(1)).thenReturn(mockUser);

        List<Account> mockAccounts = Arrays.asList(new AccountDTO(1, AccountType.CHECKING_ACCOUNT, CurrencyType.USD),
                new AccountDTO(2, AccountType.SAVINGS_ACCOUNT, CurrencyType.EUR));
        Mockito.when(accountRepository.getAccountsForUser(1)).thenReturn(mockAccounts);

        List<Account> accounts = accountService.getAccountsForUser(1);

        Assert.assertEquals(accounts, mockAccounts);
    }

    @Test(expected = NoContentException.class)
    public void getAccountsForUser_Should_ThrowException_When_UserDoesntHaveAccounts() throws SQLException {
        User mockUser = new UserDTO(1, "email@gmail.com", "1234", "1234", "Ivan", "Ivanov");
        Mockito.when(userService.getUser(1)).thenReturn(mockUser);

        List<Account> mockAccounts = new ArrayList<>();
        Mockito.when(accountRepository.getAccountsForUser(1)).thenReturn(mockAccounts);

        List<Account> accounts = accountService.getAccountsForUser(1);

        Assert.assertEquals(accounts, mockAccounts);
    }

    @Test
    public void getAccount_Should_ReturnAccount_When_AccountExists() throws SQLException {
        Account mockAccount = new AccountDTO(1, AccountType.CHECKING_ACCOUNT, CurrencyType.USD);
        Mockito.when(accountRepository.getAccount(1)).thenReturn(mockAccount);

        Account account = accountService.getAccount(1);

        Assert.assertEquals(account, mockAccount);
    }

    @Test(expected = NotExistingException.class)
    public void getAccount_Should_ThrowException_When_AccountDoesntExist() throws SQLException {
        Account mockAccount = null;
        Mockito.when(accountRepository.getAccount(1)).thenReturn(mockAccount);

        Account account = accountService.getAccount(1);

        Assert.assertEquals(account, mockAccount);
    }

    @Test
    public void createAccount_Should_Call_RepositoryCreateAccount_When_FieldsAreNotNull() throws SQLException {
        User mockUser = new UserDTO(1, "email@gmail.com", "1234", "1234", "Ivan", "Ivanov");
        Mockito.when(userService.getUser(1)).thenReturn(mockUser);

        Mockito.when(accountTypeService.getAccountTypeIdForName("CHECKING_ACCOUNT")).thenReturn(1);
        Mockito.when(currencyTypeService.getCurrencyTypeIdForName("EUR")).thenReturn(1);

        Account account = new Account(AccountType.CHECKING_ACCOUNT, CurrencyType.EUR, 1);
        accountService.createAccount(account);

        Mockito.verify(accountRepository, Mockito.times(1)).createAccount(1, 1, mockUser);
    }

    @Test(expected = NotAcceptableException.class)
    public void createAccount_Should_ThrowException_When_FieldsAreNull() throws SQLException {
        Account account = new Account(null, null, null);
        accountService.createAccount(account);

        Mockito.verify(accountRepository, Mockito.never()).createAccount(1, 1, new User());
    }
}
