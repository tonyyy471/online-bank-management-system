package online.bank.system;

import online.bank.system.exceptions.NoContentException;
import online.bank.system.exceptions.NotAcceptableException;
import online.bank.system.models.*;
import online.bank.system.models.dtos.AccountDTO;
import online.bank.system.models.dtos.TransactionDTO;
import online.bank.system.repositories.TransactionRepositoryImpl;
import online.bank.system.services.AccountServiceImpl;
import online.bank.system.services.CurrencyTypeServiceImpl;
import online.bank.system.services.TransactionServiceImpl;
import online.bank.system.services.TransactionTypeServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceImplTests {
    @Mock
    TransactionRepositoryImpl transactionRepository;
    @Mock
    AccountServiceImpl accountService;
    @Mock
    TransactionTypeServiceImpl transactionTypeService;
    @Mock
    CurrencyTypeServiceImpl currencyTypeService;
    @InjectMocks
    TransactionServiceImpl transactionService;

    @Test
    public void getTransactionsForAccount_Should_ReturnTransactionsForAccount_When_AccountHasTransactions() throws SQLException {
        Account mockAccount = new AccountDTO(1, AccountType.CHECKING_ACCOUNT, CurrencyType.USD);
        Mockito.when(accountService.getAccount(1)).thenReturn(mockAccount);

        List<Transaction> mockTransactions = Arrays.asList(
                new TransactionDTO(TransactionType.DEPOSIT, 100.0, 1, null, 1,
                        "2020-12-28 10:18:52", "Ivan", "Ivanov", null,
                        null, null),
                new TransactionDTO(TransactionType.WITHDRAWAL, 100.0, 1, null, 1,
                        "2020-12-28 10:20:10", "Ivan", "Ivanov", null,
                        null, null));
        Mockito.when(transactionRepository.getTransactionsForAccount(mockAccount)).thenReturn(mockTransactions);

        List<Transaction> transactions = transactionService.getTransactionsForAccount(1);

        Assert.assertEquals(transactions, mockTransactions);
    }

    @Test(expected = NoContentException.class)
    public void getTransactionsForAccount_Should_ThrowException_When_AccountDoesntHaveTransactions() throws SQLException {
        Account mockAccount = new AccountDTO(1, AccountType.CHECKING_ACCOUNT, CurrencyType.USD);
        Mockito.when(accountService.getAccount(1)).thenReturn(mockAccount);

        List<Transaction> mockTransactions = new ArrayList<>();
        Mockito.when(transactionRepository.getTransactionsForAccount(mockAccount)).thenReturn(mockTransactions);

        List<Transaction> transactions = transactionService.getTransactionsForAccount(1);

        Assert.assertEquals(transactions, mockTransactions);
    }

    @Test
    public void createTransaction_Should_Call_RepositoryCreateTransaction_When_TransactionIsDeposit_And_FieldsAreNotNull() throws SQLException {
        Transaction transaction = new Transaction(TransactionType.DEPOSIT, 1000.0, 1);

        Account mockAccount = new AccountDTO(1, AccountType.CHECKING_ACCOUNT, CurrencyType.EUR);
        Mockito.when(accountService.getAccount(1)).thenReturn(mockAccount);

        Mockito.when(transactionTypeService.getTransactionTypeIdForName("DEPOSIT")).thenReturn(1);

        transactionService.createTransaction(transaction);

        Mockito.verify(transactionRepository, Mockito.times(1)).createTransaction(1, transaction);
    }

    @Test
    public void createTransaction_Should_Call_RepositoryCreateTransaction_When_TransactionIsTransfer_And_ReceivingAccountIdIsNotSelf_And_AccountHasEnoughBalance() throws SQLException {
        Transaction transaction = new Transaction(TransactionType.TRANSFER, 1000.0, 1, 2);

        Account mockAccount = new AccountDTO(1, AccountType.CHECKING_ACCOUNT, CurrencyType.EUR);
        Mockito.when(accountService.getAccount(1)).thenReturn(mockAccount);

        Account mockOptionalReceivingAccount = new AccountDTO(2, AccountType.SAVINGS_ACCOUNT, CurrencyType.EUR);
        Mockito.when(accountService.getAccount(2)).thenReturn(mockOptionalReceivingAccount);

        Mockito.when(transactionTypeService.getTransactionTypeIdForName("TRANSFER")).thenReturn(2);

        List<Transaction> mockTransactions = Arrays.asList(
                new TransactionDTO(TransactionType.DEPOSIT, 1000.0, 1, null, 1,
                        "2020-12-28 10:18:52", "Ivan", "Ivanov", null,
                        null, null),
                new TransactionDTO(TransactionType.DEPOSIT, 1000.0, 1, null, 1,
                        "2020-12-28 10:20:10", "Ivan", "Ivanov", null,
                        null, null));
        Mockito.when(transactionRepository.getTransactionsForAccount(mockAccount)).thenReturn(mockTransactions);

        transactionService.createTransaction(transaction);

        Mockito.verify(transactionRepository, Mockito.times(1)).createTransaction(2, transaction);
    }

    @Test(expected = NotAcceptableException.class)
    public void createTransaction_Should_ThrowException_When_TransactionIsTransfer_And_ReceivingAccountIdIsSelf() throws SQLException {
        Transaction transaction = new Transaction(TransactionType.TRANSFER, 1000.0, 1, 1);

        Account mockAccount = new AccountDTO(1, AccountType.CHECKING_ACCOUNT, CurrencyType.EUR);
        Mockito.when(accountService.getAccount(1)).thenReturn(mockAccount);

        transactionService.createTransaction(transaction);

        Mockito.verify(transactionRepository, Mockito.never()).createTransaction(2, transaction);
    }

    @Test(expected = NotAcceptableException.class)
    public void createTransaction_Should_ThrowException_When_TransactionIsTransfer_And_AccountDoesntHaveEnoughBalance() throws SQLException {
        Transaction transaction = new Transaction(TransactionType.TRANSFER, 1000.0, 1, 2);

        Account mockAccount = new AccountDTO(1, AccountType.CHECKING_ACCOUNT, CurrencyType.EUR);
        Mockito.when(accountService.getAccount(1)).thenReturn(mockAccount);

        Account mockOptionalReceivingAccount = new AccountDTO(2, AccountType.SAVINGS_ACCOUNT, CurrencyType.EUR);
        Mockito.when(accountService.getAccount(2)).thenReturn(mockOptionalReceivingAccount);

        List<Transaction> mockTransactions = Arrays.asList(
                new TransactionDTO(TransactionType.DEPOSIT, 499.0, 1, null, 1,
                        "2020-12-28 10:18:52", "Ivan", "Ivanov", null,
                        null, null),
                new TransactionDTO(TransactionType.DEPOSIT, 500.0, 1, null, 1,
                        "2020-12-28 10:20:10", "Ivan", "Ivanov", null,
                        null, null));
        Mockito.when(transactionRepository.getTransactionsForAccount(mockAccount)).thenReturn(mockTransactions);

        transactionService.createTransaction(transaction);

        Mockito.verify(transactionRepository, Mockito.never()).createTransaction(2, transaction);
    }

    @Test(expected = NotAcceptableException.class)
    public void createTransaction_Should_ThrowException_When_FieldsAreNull() throws SQLException {
        Transaction transaction = new Transaction(null, null, null);

        transactionService.createTransaction(transaction);

        Mockito.verify(transactionRepository, Mockito.never()).createTransaction(1, transaction);
    }

    @Test
    public void getBalanceForAccount_Should_Return_Balance_When_AccountHasTransactions() throws SQLException {
        Account mockAccount = new AccountDTO(1, AccountType.CHECKING_ACCOUNT, CurrencyType.EUR);
        Mockito.when(accountService.getAccount(1)).thenReturn(mockAccount);

        List<Transaction> mockTransactions = Arrays.asList(
                new TransactionDTO(TransactionType.DEPOSIT, 499.0, 1, null, 1,
                        "2020-12-28 10:18:52", "Ivan", "Ivanov", null,
                        null, null),
                new TransactionDTO(TransactionType.DEPOSIT, 500.0, 1, null, 1,
                        "2020-12-28 10:20:10", "Ivan", "Ivanov", null,
                        null, null),
                new TransactionDTO(TransactionType.WITHDRAWAL, 200.0, 1, null, 1,
                        "2020-12-28 10:20:10", "Ivan", "Ivanov", null,
                        null, null));
        Mockito.when(transactionRepository.getTransactionsForAccount(mockAccount)).thenReturn(mockTransactions);

        double balance = transactionService.getBalanceForAccount(1);

        Assert.assertEquals(balance, 799.0, 0);
    }
}
